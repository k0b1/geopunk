<div class="geopunk-c-admin-sidebar__container">
  <ul class="geopunk-c-admin-sidebar__list">
    <li class="geopunk-c-admin-sidebar__item">
      <a href="{{ url('/user/1/my-profile') }}">Your profile</a></li>
    <li class="">
      <a href="{{ url('/user/upload-band') }}">Upload band</a></li>
    <li class="">
      <a href="{{ url('/user/publish-show') }}">Publish show</a></li>
    <li class="">
      <a href="{{ url('/user/your-comments') }}">Your comments</a></li>
    <li class="">
      <a href="{{ url('/user/your-uploads') }}">Your uploads</a></li>
  </ul>    
</div>
