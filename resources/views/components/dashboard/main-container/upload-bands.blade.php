<div class="geopunk-c-admin-dashboard__container">
  @if ( $errors->any() )
    <div>
      <div class="alert alert-danger">
        <ul>
          @foreach( $errors->all() as $error )
            <li>{{ $error  }}}}</li>
          @endforeach
        </ul>
      </div>
    </div>
  @endif

  <h2>Please upload bands you like!</h2>
  <form action="">
    <div>
      <label for="band-name">
        Enter the band name:
      </label>
      <input name="band-name" type="text" value="" placeholder="Write down the band name" />   
    </div>
    <div>
      <label for="band-release">
        Enter the band release:
      </label>
      <input name="band-release" type="text" value="" placeholder="Write down the band release" />
   
    </div>
   
    <input name="upload-band" type="file" value=""/>
  </form>
</div>
