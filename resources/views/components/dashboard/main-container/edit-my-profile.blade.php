<div class="geopunk-c-admin-dashboard__container">
  This is the place for different inputs
  <form action="/user/1/edit-profile-form" method="POST">
    @csrf
    <div class="form-group">
      <label for="exampleFormControlInput1">Email address</label>
      <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="Example: name@example.com" name="user-email">
    </div>
    <div class="form-group">
      Your password: <input name="user-password" type="password">
      Repeat password: <input name="user-password-second" type="password">
    </div>
    <div class="form-group">
      Your city: <input name="user-city" type="text">
      Your country: <input name="user-country" type="text">
    </div>
    <div class="form-group">
      Your bands: <input name="user-bands" type="text">
    </div>
    <input type="submit" value="Save" />
  </form>
</div>
