@extends('layouts.app')

@section('content')
  @if (session('status'))
    <div class="alert alert-success" role="alert">
      {{ session('status') }}
    </div>
  @endif
  <div class="geopunk-c-admin-main-container">
    @component('components.dashboard.user-profile-sidebar')
    @endcomponent
    
    @component('components.dashboard.main-container.edit-my-profile')
    @endcomponent
  </div>
@endsection

