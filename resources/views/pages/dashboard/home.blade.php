@extends('layouts.app')

@section('content')
<div class="geopunk-c-admin-main-container">
  @component('components.dashboard.user-profile-sidebar')
  @endcomponent
  
  <div class="geopunk-c-admin-dashboard__container">
    <h2>You are logged in!</h2>

   Welcome to Geopun admin. Select option from the sidebar on the right sight and start using Geopunk app.
  </div>  
</div>
@endsection
