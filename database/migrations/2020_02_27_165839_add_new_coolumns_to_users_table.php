<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewCoolumnsToUsersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    // add new columns to Users table
    Schema::table('users', function(Blueprint $table){
      $table->string('city')->after('name');
      $table->string('country')->after('city');
      $table->string('activity')->after('country'); // user activity on punk scene, bands, fanzines, etc
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('users', function (Blueprint $table) {
      $table->dropColumn('city');
      $table->dropColumn('country');
      $table->dropColumn('activity'); // user activity on punk scene, bands, fanzines, etc
    });
  }
}
