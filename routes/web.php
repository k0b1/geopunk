<?php

/*
   |--------------------------------------------------------------------------
   | Web Routes
   |--------------------------------------------------------------------------
   |
   | Here is where you can register web routes for your application. These
   | routes are loaded by the RouteServiceProvider within a group which
   | contains the "web" middleware group. Now create something great!
   |
 */

Route::get('/', function () {
  return view('welcome', ['name'=>'Bobi']);
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/user/{id}/my-profile', 'UserControler@user_dashboard')->name('my-profile');

Route::post('/user/{id}/edit-profile-form', 'EditUserController@edit_user_dashboard')->name('my-profile');
